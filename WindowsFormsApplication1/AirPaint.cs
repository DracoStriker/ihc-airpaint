﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.Kinect;

namespace Air
{
    public partial class AirPaint : Form
    {
        private const uint MOUSEEVENTF_LEFTDOWN = 0x0002;
        private const uint MOUSEEVENTF_LEFTUP = 0x0004;
        private const int MAX_LIST_SIZE = 10;
        private const int threashouldX = 18, threashouldY = 30;

        [DllImport("user32.dll")]
        private static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint dwData, uint dwExtraInf);
        
        private Graphics graph;
        private Pen pen;
        private Point ip, fp;
        private KinectSensor sensor;
        private LinkedList<Image> previousList;
        private PictureBox currentBox;
        private SaveFileDialog sfd;
        private bool clicked, rubber, pressed;

        public AirPaint()
        {
            InitializeComponent();
            WindowsDimension();
            ControlBox = false;
            canvasPanel.Image = new Bitmap(canvasPanel.Width, canvasPanel.Height);
            graph = Graphics.FromImage(canvasPanel.Image);
            graph.Clear(Color.White);
            pen = new Pen(Color.Black, 1.0f);
            ip = new Point(0, 0);
            fp = new Point(0, 0);
            clicked = false;
            rubber = false;
            pressed = false;
            FormBorderStyle = FormBorderStyle.FixedDialog;
            previousList = new LinkedList<Image>();
            pencilBox.BackColor = Color.SkyBlue;
            currentBox = pencilBox;
            FormBorderStyle = FormBorderStyle.None;
        }

        private void WindowsDimension()
        {
            PictureBox[] colorBoxes;
            PictureBox previous = null;
            int newX = 0, newY = 0, addX;
            if (Screen.PrimaryScreen.Bounds.Width > 1128)
            {
                newX = Screen.PrimaryScreen.Bounds.Width - 1128 + threashouldX;
                panel3.Location = new Point(panel3.Location.X + newX, panel3.Location.Y);
            }
            if (Screen.PrimaryScreen.Bounds.Height > 621)
            {
                newY = Screen.PrimaryScreen.Bounds.Height - 621 + threashouldY;
                panel1.Location = new Point(panel1.Location.X, panel1.Location.Y + newY);
            }
            panel1.Size = new Size(panel1.Size.Width + newX, panel1.Size.Height);
            selectBox.Location = new Point(selectBox.Location.X + newX, selectBox.Location.Y);
            addX = newX / 10;
            if (addX > 0)
            {
                colorBoxes = new PictureBox[10] { blackBox, grayBox, whiteBox, redBox, orangeBox, yellowBox, greenBox, cyanBox, blueBox, purpleBox};
                foreach (PictureBox pb in colorBoxes)
                {
                    pb.Size = new Size(pb.Width + addX, pb.Height);
                    if (pb != colorBoxes[0])
                    {
                        pb.Location = new Point(previous.Location.X + 80 + addX, previous.Location.Y);
                    }
                    previous = pb;
                }
            }
            canvasPanel.Size = new Size(canvasPanel.Size.Width + newX, canvasPanel.Size.Height + newY);
        }

        private void InitializeKinect()
        {
            try
            {
                foreach (var kSensor in KinectSensor.KinectSensors)
                {
                    Console.WriteLine(kSensor.Status);
                }
                sensor = KinectSensor.KinectSensors[0];
                sensor.SkeletonStream.Enable();
                sensor.Start();
                sensor.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(kinectSensor_SkeletonFrameReady);
            }
            catch (ArgumentOutOfRangeException)
            {
                if (sensor != null)
                {
                    sensor.Stop();
                    sensor = null;
                }
                new Warning("Kinect Device Unplugged.").Show();
            }
            catch (NullReferenceException)
            {
                if (sensor != null)
                {
                    sensor.Stop();
                    sensor = null;
                }
                new Warning("Couldn't Connect to Kinect Device.").Show();
            }
        }

        void kinectSensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    Skeleton[] skeletonData = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletonData);
                    Skeleton playerSkeleton = (from s in skeletonData where s.TrackingState == SkeletonTrackingState.Tracked select s).FirstOrDefault();
                    if (playerSkeleton != null)
                    {
                        Joint rightHand = playerSkeleton.Joints[JointType.HandRight];
                        Joint leftHand = playerSkeleton.Joints[JointType.HandLeft];
                        Joint centerShoulder = playerSkeleton.Joints[JointType.ShoulderCenter];
                        System.Windows.Forms.Cursor.Position = new Point(Location.X + Bounds.Width / 2 + (int)((rightHand.Position.X - 0.2f) * Bounds.Width / 0.4f), Location.Y - (int)((rightHand.Position.Y - 0.15f) * Bounds.Height / 0.25));
                        if ((leftHand.Position.Z < centerShoulder.Position.Z) && ((centerShoulder.Position.Z - leftHand.Position.Z) > 0.4f) && !pressed)
                        {
                            mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                            pressed = true;
                        }
                        else if ((leftHand.Position.Z < centerShoulder.Position.Z) && ((centerShoulder.Position.Z - leftHand.Position.Z) < 0.4f) && pressed)
                        {
                            mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                            pressed = false;
                        }
                    }
                }
            }
        }

        private void redBox_Click(object sender, EventArgs e)
        {
            pen.Color = redBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void orangeBox_Click(object sender, EventArgs e)
        {
            pen.Color = orangeBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void yellowBox_Click(object sender, EventArgs e)
        {
            pen.Color = yellowBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void greenBox_Click(object sender, EventArgs e)
        {
            pen.Color = greenBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void cyanBox_Click(object sender, EventArgs e)
        {
            pen.Color = cyanBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void blueBox_Click(object sender, EventArgs e)
        {
            pen.Color = blueBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void purpleBox_Click(object sender, EventArgs e)
        {
            pen.Color = purpleBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void canvasPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (previousList.Count >= MAX_LIST_SIZE)
                {
                    previousList.RemoveFirst();
                }
                else
                {
                    previousList.AddLast(new Bitmap(canvasPanel.Image));
                }
                ip = e.Location;
                clicked = true;
            }
        }

        private void canvasPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                clicked = false;
            }
        }

        private void canvasPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (clicked)
            {
                if (rubber)
                {
                    graph.FillRectangle(Brushes.White, e.Location.X-25, e.Location.Y-25, 50, 50);
                }
                else
                {
                    fp = e.Location;
                    graph.DrawLine(pen, ip, fp);
                    ip = fp;
                }
                canvasPanel.Refresh();
            }
        }

        private void newBox_Click(object sender, EventArgs e)
        {
            if (previousList.Count < MAX_LIST_SIZE)
            {
                if (previousList.Count >= MAX_LIST_SIZE)
                {
                    previousList.RemoveFirst();
                }
                else
                {
                    previousList.AddLast(new Bitmap(canvasPanel.Image));
                }
            }
            canvasPanel.Image = new Bitmap(canvasPanel.Width, canvasPanel.Height);
            graph = Graphics.FromImage(canvasPanel.Image);
            graph.Clear(Color.White);
            canvasPanel.Refresh();
        }

        private void openBox_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg_Open = new OpenFileDialog();
            dlg_Open.Filter = "Image File|*.jpg;*.png;*.bmp|BMP|*.bmp|PNG|*.png|JPG|*.jpg";
            dlg_Open.FileName = "";
            if (dlg_Open.ShowDialog() == DialogResult.OK)
            {
                while (previousList.Count > 0)
                {
                    previousList.RemoveLast();
                }
                canvasPanel.Image = Image.FromFile(dlg_Open.FileName);
                graph = Graphics.FromImage(canvasPanel.Image);
            }
        }

        private void blackBox_Click(object sender, EventArgs e)
        {
            pen.Color = blackBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void whiteBox_Click(object sender, EventArgs e)
        {
            pen.Color = whiteBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void saveBox_Click(object sender, EventArgs e)
        {
            sfd = new SaveFileDialog();
            sfd.Title = "AirPaint Save Image";
            sfd.FileName = "air_paint_" + new Random().Next(1000000);
            sfd.Filter = "BMP|*.bmp|PNG|*.png|JPG|*.jpg";
            sfd.FileOk += saveButton_Click;
            sfd.ShowDialog();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            ImageFormat format;
            switch (sfd.FilterIndex)
            {
                case 1:
                    format = ImageFormat.Bmp;
                    break;
                case 2:
                    format = ImageFormat.Png;
                    break;
                case 3:
                    format = ImageFormat.Jpeg;
                    break;
                default:
                    format = ImageFormat.Png;
                    break;
            }
            canvasPanel.Image.Save(sfd.FileName, format);
        }

        private void exitBox_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pencilBox_Click(object sender, EventArgs e)
        {
            if (rubber)
            {
                pen.Color = selectBox.BackColor;
            }
            pen.Width = 1.0f;
            rubber = false;
            if (currentBox != null)
            {
                currentBox.BackColor = Color.Transparent;
            }
            pencilBox.BackColor = Color.SkyBlue;
            currentBox = pencilBox;
        }

        private void brushBox_Click(object sender, EventArgs e)
        {
            if (rubber)
            {
                pen.Color = selectBox.BackColor;
            }
            pen.Width = 5.0f;
            rubber = false;
            if (currentBox != null)
            {
                currentBox.BackColor = Color.Transparent;
            }
            brushBox.BackColor = Color.SkyBlue;
            currentBox = brushBox;
        }

        private void rubberBox_Click(object sender, EventArgs e)
        {
            pen.Color = Color.White;
            rubber = true;
            if (currentBox != null)
            {
                currentBox.BackColor = Color.Transparent;
            }
            rubberBox.BackColor = Color.SkyBlue;
            currentBox = rubberBox;
        }

        private void giantBrushBox_Click(object sender, EventArgs e)
        {
            if (rubber)
            {
                pen.Color = selectBox.BackColor;
            }
            pen.Width = 10.0f;
            rubber = false;
            if (currentBox != null)
            {
                currentBox.BackColor = Color.Transparent;
            }
            giantBrushBox.BackColor = Color.SkyBlue;
            currentBox = giantBrushBox;
        }

        private void AirPaint_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (sensor != null)
            {
                sensor.Stop();
            }
        }

        private void undoBox_Click(object sender, EventArgs e)
        {
            if (previousList.Count != 0)
            {
                canvasPanel.Image = previousList.Last();
                previousList.RemoveLast();
                graph = Graphics.FromImage(canvasPanel.Image);
                canvasPanel.Refresh();
            }
        }

        private void grayBox_Click(object sender, EventArgs e)
        {
            pen.Color = grayBox.BackColor;
            selectBox.BackColor = pen.Color;
        }

        private void AirPaint_Load(object sender, EventArgs e)
        {
            Bounds = Screen.PrimaryScreen.Bounds;
            MaximizeBox = true;
        }

        private void kinectBox_Click(object sender, EventArgs e)
        {
            if (sensor == null)
            {
                InitializeKinect();
            }
            else
            {
                if (sensor.Status == KinectStatus.Connected)
                {
                    new Warning("Kinect Device Already Plugged In.").Show();
                }
                else
                {
                    InitializeKinect();
                }
            }
        }

        private void AirPaint_Shown(object sender, EventArgs e)
        {
            InitializeKinect();
        }
    }
}
