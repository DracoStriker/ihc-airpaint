﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Kinect;

namespace Air
{
    public partial class Warning : Form
    {
        public Warning(String text)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            warningBox.Text = text;
        }

        private void Warning_Load(object sender, EventArgs e)
        {
            Location = new Point((Screen.PrimaryScreen.Bounds.Width - Bounds.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - Bounds.Height) / 2);
        }
    }
}
