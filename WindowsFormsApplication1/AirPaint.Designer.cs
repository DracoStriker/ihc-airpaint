﻿namespace Air
{
    partial class AirPaint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.grayBox = new System.Windows.Forms.PictureBox();
            this.selectBox = new System.Windows.Forms.PictureBox();
            this.whiteBox = new System.Windows.Forms.PictureBox();
            this.blackBox = new System.Windows.Forms.PictureBox();
            this.purpleBox = new System.Windows.Forms.PictureBox();
            this.blueBox = new System.Windows.Forms.PictureBox();
            this.cyanBox = new System.Windows.Forms.PictureBox();
            this.greenBox = new System.Windows.Forms.PictureBox();
            this.yellowBox = new System.Windows.Forms.PictureBox();
            this.orangeBox = new System.Windows.Forms.PictureBox();
            this.redBox = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.kinectBox = new System.Windows.Forms.PictureBox();
            this.openBox = new System.Windows.Forms.PictureBox();
            this.saveBox = new System.Windows.Forms.PictureBox();
            this.exitBox = new System.Windows.Forms.PictureBox();
            this.newBox = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.undoBox = new System.Windows.Forms.PictureBox();
            this.giantBrushBox = new System.Windows.Forms.PictureBox();
            this.rubberBox = new System.Windows.Forms.PictureBox();
            this.brushBox = new System.Windows.Forms.PictureBox();
            this.pencilBox = new System.Windows.Forms.PictureBox();
            this.canvasPanel = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grayBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.purpleBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyanBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yellowBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orangeBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redBox)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kinectBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newBox)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.undoBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.giantBrushBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rubberBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brushBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pencilBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grayBox);
            this.panel1.Controls.Add(this.selectBox);
            this.panel1.Controls.Add(this.whiteBox);
            this.panel1.Controls.Add(this.blackBox);
            this.panel1.Controls.Add(this.purpleBox);
            this.panel1.Controls.Add(this.blueBox);
            this.panel1.Controls.Add(this.cyanBox);
            this.panel1.Controls.Add(this.greenBox);
            this.panel1.Controls.Add(this.yellowBox);
            this.panel1.Controls.Add(this.orangeBox);
            this.panel1.Controls.Add(this.redBox);
            this.panel1.Location = new System.Drawing.Point(100, 498);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(913, 81);
            this.panel1.TabIndex = 0;
            // 
            // grayBox
            // 
            this.grayBox.BackColor = System.Drawing.Color.Gray;
            this.grayBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.grayBox.Location = new System.Drawing.Point(81, 3);
            this.grayBox.Name = "grayBox";
            this.grayBox.Size = new System.Drawing.Size(75, 75);
            this.grayBox.TabIndex = 10;
            this.grayBox.TabStop = false;
            this.grayBox.Click += new System.EventHandler(this.grayBox_Click);
            // 
            // selectBox
            // 
            this.selectBox.BackColor = System.Drawing.Color.Black;
            this.selectBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.selectBox.Location = new System.Drawing.Point(838, 3);
            this.selectBox.Name = "selectBox";
            this.selectBox.Size = new System.Drawing.Size(75, 75);
            this.selectBox.TabIndex = 15;
            this.selectBox.TabStop = false;
            // 
            // whiteBox
            // 
            this.whiteBox.BackColor = System.Drawing.Color.White;
            this.whiteBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.whiteBox.Location = new System.Drawing.Point(162, 3);
            this.whiteBox.Name = "whiteBox";
            this.whiteBox.Size = new System.Drawing.Size(75, 75);
            this.whiteBox.TabIndex = 9;
            this.whiteBox.TabStop = false;
            this.whiteBox.Click += new System.EventHandler(this.whiteBox_Click);
            // 
            // blackBox
            // 
            this.blackBox.BackColor = System.Drawing.Color.Black;
            this.blackBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.blackBox.Location = new System.Drawing.Point(0, 3);
            this.blackBox.Name = "blackBox";
            this.blackBox.Size = new System.Drawing.Size(75, 75);
            this.blackBox.TabIndex = 8;
            this.blackBox.TabStop = false;
            this.blackBox.Click += new System.EventHandler(this.blackBox_Click);
            // 
            // purpleBox
            // 
            this.purpleBox.BackColor = System.Drawing.Color.Fuchsia;
            this.purpleBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.purpleBox.Location = new System.Drawing.Point(729, 3);
            this.purpleBox.Name = "purpleBox";
            this.purpleBox.Size = new System.Drawing.Size(75, 75);
            this.purpleBox.TabIndex = 6;
            this.purpleBox.TabStop = false;
            this.purpleBox.Click += new System.EventHandler(this.purpleBox_Click);
            // 
            // blueBox
            // 
            this.blueBox.BackColor = System.Drawing.Color.Blue;
            this.blueBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.blueBox.Location = new System.Drawing.Point(648, 3);
            this.blueBox.Name = "blueBox";
            this.blueBox.Size = new System.Drawing.Size(75, 75);
            this.blueBox.TabIndex = 5;
            this.blueBox.TabStop = false;
            this.blueBox.Click += new System.EventHandler(this.blueBox_Click);
            // 
            // cyanBox
            // 
            this.cyanBox.BackColor = System.Drawing.Color.Aqua;
            this.cyanBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cyanBox.Location = new System.Drawing.Point(567, 3);
            this.cyanBox.Name = "cyanBox";
            this.cyanBox.Size = new System.Drawing.Size(75, 75);
            this.cyanBox.TabIndex = 4;
            this.cyanBox.TabStop = false;
            this.cyanBox.Click += new System.EventHandler(this.cyanBox_Click);
            // 
            // greenBox
            // 
            this.greenBox.BackColor = System.Drawing.Color.Lime;
            this.greenBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.greenBox.Location = new System.Drawing.Point(486, 3);
            this.greenBox.Name = "greenBox";
            this.greenBox.Size = new System.Drawing.Size(75, 75);
            this.greenBox.TabIndex = 3;
            this.greenBox.TabStop = false;
            this.greenBox.Click += new System.EventHandler(this.greenBox_Click);
            // 
            // yellowBox
            // 
            this.yellowBox.BackColor = System.Drawing.Color.Yellow;
            this.yellowBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.yellowBox.Location = new System.Drawing.Point(405, 3);
            this.yellowBox.Name = "yellowBox";
            this.yellowBox.Size = new System.Drawing.Size(75, 75);
            this.yellowBox.TabIndex = 2;
            this.yellowBox.TabStop = false;
            this.yellowBox.Click += new System.EventHandler(this.yellowBox_Click);
            // 
            // orangeBox
            // 
            this.orangeBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.orangeBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.orangeBox.Location = new System.Drawing.Point(324, 3);
            this.orangeBox.Name = "orangeBox";
            this.orangeBox.Size = new System.Drawing.Size(75, 75);
            this.orangeBox.TabIndex = 1;
            this.orangeBox.TabStop = false;
            this.orangeBox.Click += new System.EventHandler(this.orangeBox_Click);
            // 
            // redBox
            // 
            this.redBox.BackColor = System.Drawing.Color.Red;
            this.redBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.redBox.Location = new System.Drawing.Point(243, 3);
            this.redBox.Name = "redBox";
            this.redBox.Size = new System.Drawing.Size(75, 75);
            this.redBox.TabIndex = 0;
            this.redBox.TabStop = false;
            this.redBox.Click += new System.EventHandler(this.redBox_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.kinectBox);
            this.panel3.Controls.Add(this.newBox);
            this.panel3.Controls.Add(this.openBox);
            this.panel3.Controls.Add(this.saveBox);
            this.panel3.Controls.Add(this.exitBox);
            this.panel3.Location = new System.Drawing.Point(1015, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(85, 567);
            this.panel3.TabIndex = 6;
            // 
            // kinectBox
            // 
            this.kinectBox.BackColor = System.Drawing.SystemColors.Control;
            this.kinectBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.kinectBox.Image = global::Air.Properties.Resources.rsz_kinect;
            this.kinectBox.Location = new System.Drawing.Point(10, 325);
            this.kinectBox.Name = "kinectBox";
            this.kinectBox.Size = new System.Drawing.Size(75, 75);
            this.kinectBox.TabIndex = 14;
            this.kinectBox.TabStop = false;
            this.kinectBox.Click += new System.EventHandler(this.kinectBox_Click);
            // 
            // openBox
            // 
            this.openBox.BackColor = System.Drawing.SystemColors.Control;
            this.openBox.BackgroundImage = global::Air.Properties.Resources.rsz_open_icon;
            this.openBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.openBox.Location = new System.Drawing.Point(10, 162);
            this.openBox.Name = "openBox";
            this.openBox.Size = new System.Drawing.Size(75, 75);
            this.openBox.TabIndex = 12;
            this.openBox.TabStop = false;
            this.openBox.Click += new System.EventHandler(this.openBox_Click);
            // 
            // saveBox
            // 
            this.saveBox.BackColor = System.Drawing.SystemColors.Control;
            this.saveBox.BackgroundImage = global::Air.Properties.Resources.rsz_save_icon;
            this.saveBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.saveBox.Location = new System.Drawing.Point(10, 81);
            this.saveBox.Name = "saveBox";
            this.saveBox.Size = new System.Drawing.Size(75, 75);
            this.saveBox.TabIndex = 11;
            this.saveBox.TabStop = false;
            this.saveBox.Click += new System.EventHandler(this.saveBox_Click);
            // 
            // exitBox
            // 
            this.exitBox.BackColor = System.Drawing.SystemColors.Control;
            this.exitBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBox.Image = global::Air.Properties.Resources.rsz_exit_icon;
            this.exitBox.Location = new System.Drawing.Point(10, 0);
            this.exitBox.Name = "exitBox";
            this.exitBox.Size = new System.Drawing.Size(75, 75);
            this.exitBox.TabIndex = 10;
            this.exitBox.TabStop = false;
            this.exitBox.Click += new System.EventHandler(this.exitBox_Click);
            // 
            // newBox
            // 
            this.newBox.BackColor = System.Drawing.SystemColors.Control;
            this.newBox.BackgroundImage = global::Air.Properties.Resources.rsz_1368852314_edit_clear;
            this.newBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newBox.Location = new System.Drawing.Point(10, 243);
            this.newBox.Name = "newBox";
            this.newBox.Size = new System.Drawing.Size(75, 75);
            this.newBox.TabIndex = 13;
            this.newBox.TabStop = false;
            this.newBox.Click += new System.EventHandler(this.newBox_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.undoBox);
            this.panel2.Controls.Add(this.giantBrushBox);
            this.panel2.Controls.Add(this.rubberBox);
            this.panel2.Controls.Add(this.brushBox);
            this.panel2.Controls.Add(this.pencilBox);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(85, 567);
            this.panel2.TabIndex = 8;
            // 
            // undoBox
            // 
            this.undoBox.BackColor = System.Drawing.SystemColors.Control;
            this.undoBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.undoBox.Image = global::Air.Properties.Resources.rsz_undo_icon;
            this.undoBox.Location = new System.Drawing.Point(0, 325);
            this.undoBox.Name = "undoBox";
            this.undoBox.Size = new System.Drawing.Size(75, 75);
            this.undoBox.TabIndex = 16;
            this.undoBox.TabStop = false;
            this.undoBox.Click += new System.EventHandler(this.undoBox_Click);
            // 
            // giantBrushBox
            // 
            this.giantBrushBox.BackColor = System.Drawing.SystemColors.Control;
            this.giantBrushBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.giantBrushBox.Image = global::Air.Properties.Resources.rsz_giant_brush_icon;
            this.giantBrushBox.Location = new System.Drawing.Point(0, 162);
            this.giantBrushBox.Name = "giantBrushBox";
            this.giantBrushBox.Size = new System.Drawing.Size(75, 75);
            this.giantBrushBox.TabIndex = 14;
            this.giantBrushBox.TabStop = false;
            this.giantBrushBox.Click += new System.EventHandler(this.giantBrushBox_Click);
            // 
            // rubberBox
            // 
            this.rubberBox.BackColor = System.Drawing.SystemColors.Control;
            this.rubberBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rubberBox.Image = global::Air.Properties.Resources.rsz_rubber_icon;
            this.rubberBox.Location = new System.Drawing.Point(0, 243);
            this.rubberBox.Name = "rubberBox";
            this.rubberBox.Size = new System.Drawing.Size(75, 75);
            this.rubberBox.TabIndex = 13;
            this.rubberBox.TabStop = false;
            this.rubberBox.Click += new System.EventHandler(this.rubberBox_Click);
            // 
            // brushBox
            // 
            this.brushBox.BackColor = System.Drawing.SystemColors.Control;
            this.brushBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.brushBox.Image = global::Air.Properties.Resources.rsz_brush_icon;
            this.brushBox.Location = new System.Drawing.Point(0, 81);
            this.brushBox.Name = "brushBox";
            this.brushBox.Size = new System.Drawing.Size(75, 75);
            this.brushBox.TabIndex = 12;
            this.brushBox.TabStop = false;
            this.brushBox.Click += new System.EventHandler(this.brushBox_Click);
            // 
            // pencilBox
            // 
            this.pencilBox.BackColor = System.Drawing.SystemColors.Control;
            this.pencilBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pencilBox.Image = global::Air.Properties.Resources.rsz_pencil_icon;
            this.pencilBox.Location = new System.Drawing.Point(0, 0);
            this.pencilBox.Name = "pencilBox";
            this.pencilBox.Size = new System.Drawing.Size(75, 75);
            this.pencilBox.TabIndex = 11;
            this.pencilBox.TabStop = false;
            this.pencilBox.Click += new System.EventHandler(this.pencilBox_Click);
            // 
            // canvasPanel
            // 
            this.canvasPanel.Cursor = System.Windows.Forms.Cursors.Cross;
            this.canvasPanel.Location = new System.Drawing.Point(100, 12);
            this.canvasPanel.Name = "canvasPanel";
            this.canvasPanel.Size = new System.Drawing.Size(913, 480);
            this.canvasPanel.TabIndex = 7;
            this.canvasPanel.TabStop = false;
            this.canvasPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvasPanel_MouseDown);
            this.canvasPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvasPanel_MouseMove);
            this.canvasPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvasPanel_MouseUp);
            // 
            // AirPaint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 582);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.canvasPanel);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "AirPaint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "AirPaint";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AirPaint_FormClosed);
            this.Load += new System.EventHandler(this.AirPaint_Load);
            this.Shown += new System.EventHandler(this.AirPaint_Shown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grayBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whiteBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blackBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.purpleBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.blueBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cyanBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yellowBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orangeBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redBox)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kinectBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newBox)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.undoBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.giantBrushBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rubberBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brushBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pencilBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.canvasPanel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox purpleBox;
        private System.Windows.Forms.PictureBox blueBox;
        private System.Windows.Forms.PictureBox cyanBox;
        private System.Windows.Forms.PictureBox greenBox;
        private System.Windows.Forms.PictureBox yellowBox;
        private System.Windows.Forms.PictureBox orangeBox;
        private System.Windows.Forms.PictureBox redBox;
        private System.Windows.Forms.PictureBox whiteBox;
        private System.Windows.Forms.PictureBox blackBox;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox newBox;
        private System.Windows.Forms.PictureBox openBox;
        private System.Windows.Forms.PictureBox saveBox;
        private System.Windows.Forms.PictureBox exitBox;
        private System.Windows.Forms.PictureBox canvasPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pencilBox;
        private System.Windows.Forms.PictureBox rubberBox;
        private System.Windows.Forms.PictureBox brushBox;
        private System.Windows.Forms.PictureBox giantBrushBox;
        private System.Windows.Forms.PictureBox selectBox;
        private System.Windows.Forms.PictureBox undoBox;
        private System.Windows.Forms.PictureBox grayBox;
        private System.Windows.Forms.PictureBox kinectBox;
    }
}

