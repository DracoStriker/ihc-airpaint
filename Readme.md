3rd Year Course - Human-Computer Interface
-------------------------------------------

Project Members
---------------
Simão Reis 

Hugo Frade

Project Description
-------------------
Paint application controlled via gestures with a Kinect sensor as input.
The user has multiple buttons where it can select the color, pencil type and other options.
To press the button or the pencil, the user must push forward its arm.

Implementation
--------------
C# Windows Application.
Kinect 1 API.